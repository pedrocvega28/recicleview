package com.example.reclyclerviewymodeladodedatos;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MiAdaptador extends RecyclerView.Adapter<MiAdaptador.ViewHolder> implements View.OnClickListener, Filterable {
    protected ArrayList<Alumno> listaAlumnos;
    private ArrayList<Alumno> nFilteredAlumno; // Variable para SearchView
    //private Filter alumnoFilter;
    private View.OnClickListener listener;
    private Context context;
    private LayoutInflater inflater;

    public MiAdaptador(ArrayList<Alumno> listaAlumnos, Context context) {
        this.listaAlumnos = listaAlumnos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.nFilteredAlumno = new ArrayList<>(listaAlumnos); // Asignar la lista completa
        //this.alumnoFilter = new AlumnoFilter();
    }

    public void filtrar(String texto) {
        listaAlumnos.clear();

        if (texto.isEmpty()) {
            listaAlumnos.addAll(nFilteredAlumno);
        } else {
            texto = texto.toLowerCase();
            for (Alumno alumno : nFilteredAlumno) {
                // Verificar si el nombre del alumno contiene el texto de búsqueda
                if (alumno.getMatricula().toLowerCase().contains(texto) ||
                        alumno.getNombre().toLowerCase().contains(texto) ||
                        alumno.getCarrera().toLowerCase().contains(texto)) {
                    listaAlumnos.add(alumno);  // Agregar el alumno a la lista
                }
            }
        }

        notifyDataSetChanged();  // Notificar al RecyclerView que los datos han cambiado
    }

    @NonNull
    @NotNull
    @Override
    public MiAdaptador.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MiAdaptador.ViewHolder holder, int position) {
        Alumno alumno = listaAlumnos.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.txtCarrera.setText(alumno.getCarrera());

        if (alumno.getImg() != null) {
            holder.idImagen.setImageURI(Uri.parse(alumno.getImg()));
        } else {
            // Establecer una imagen de reemplazo o dejarlo vacío según sea necesario
            holder.idImagen.setImageDrawable(null);
        }
    }

    @Override
    public int getItemCount() { return listaAlumnos != null ? listaAlumnos.size() : 0; }

    public void setOnClickListener(View.OnClickListener listener) { this.listener = listener; }

    @Override
    public void onClick(View v) { if(listener != null) listener.onClick(v); }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Alumno> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(nFilteredAlumno);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (Alumno alumnoF : nFilteredAlumno) {
                        if (alumnoF.getMatricula().toLowerCase().contains(filterPattern) ||
                                alumnoF.getNombre().toLowerCase().contains(filterPattern) ||
                                alumnoF.getCarrera().toLowerCase().contains(filterPattern)) {
                            filteredList.add(alumnoF);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaAlumnos.clear();
                listaAlumnos.addAll((ArrayList<Alumno>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtNombre = (TextView) itemView.findViewById(R.id.txtNombreAlumno);
            txtMatricula = (TextView) itemView.findViewById(R.id.txtMatricula);
            txtCarrera = (TextView) itemView.findViewById(R.id.txtCarrera);

            idImagen = (ImageView) itemView.findViewById(R.id.foto);
        }
    }
}
